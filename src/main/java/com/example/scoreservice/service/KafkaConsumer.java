package com.example.scoreservice.service;

import com.example.scoreservice.controller.ScoreController;
import com.example.scoreservice.model.Progress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {
    @Autowired
    private ScoreController scoreController;

    @KafkaListener(topics = "kafka", groupId = "group_id")
    public void consume(String message) {
        System.out.println("Consumed msg: "+ message);
    }

    @KafkaListener(topics="progress", groupId = "group_id_json", containerFactory="jsonKafkaListenerContainerFactory")
    public void consumeJson(Progress progress) {
        scoreController.update(progress.getGoal().getUserId());
        System.out.println("consumed json: "+ progress.toString());
    }
}
