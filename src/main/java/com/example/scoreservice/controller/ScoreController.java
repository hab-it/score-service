package com.example.scoreservice.controller;

import com.example.scoreservice.model.Score;
import com.example.scoreservice.repository.ScoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("/score")
public class ScoreController {
    @Autowired
    private ScoreRepository repository;

    @RequestMapping(method = GET)
    public List<Score> findAll() {
        return repository.findAll();
    }

    public ResponseEntity<String> update(String userId) {
        try {
            Optional<Score> updatedProgress = repository.findByUserId(userId).map(score -> {
                score.setValue(score.getValue() + 3);
                return repository.save(score);
            });
        }
        catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<String>("Score has been succesfully updated", HttpStatus.OK);
    }
}
