package com.example.scoreservice.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
public class Goal {
    private Long id;

    private String userId;

    private String value;
}
