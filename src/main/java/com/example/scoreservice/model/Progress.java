package com.example.scoreservice.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

@Getter
@Setter
public class Progress {
    private Long id;

    private Goal goal;

    private String date;

    private String value;
}
